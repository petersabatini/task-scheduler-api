import express from 'express';
import bodyParser from 'body-parser';
import { TaskService } from './services/TaskService';

const app = express();
const port = 3001;

app.use(bodyParser.json({limit: '25mb'}));

app.get('/tasks', async (req: any, res: any) => {
  const tasks = await TaskService.getAll();
  res.send(tasks);
});

app.post('/tasks', async (req: any, res: any) => {
  await TaskService.upsertTask(req.body);
  res.send();
});

app.get('/tasks/:id', async (req: any, res: any) => {
  const task = await TaskService.getTask(req.params.id);
  res.send(task);
});

app.put('/tasks/:id', async (req: any, res: any) => {
  const task = await TaskService.upsertTask(req.body);
  res.send(task);
});

app.delete('/tasks/:id', async (req: any, res: any) => {
  const task = await TaskService.deleteTask(req.params.id);
  res.send(task);
});

app.get('/runs', async (req: any, res: any) => {
  const runs = await TaskService.getHistoryRuns();
  res.send(runs);
});

app.get('/', (req: any, res: any) => {
  res.send();
});

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});

