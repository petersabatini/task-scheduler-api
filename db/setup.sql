CREATE DATABASE task_scheduler;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE task_crons(
  task_id uuid primary key DEFAULT uuid_generate_v4(),
  name text NOT NULL,
  type text NOT NULL CHECK(type in ('one-time', 'recurring')),
  schedule text,
  runtime timestamp,
  action text NOT NULL CHECK(action in ('notification', 'procedure')),
  params jsonb NOT NULL,
  create_date timestamp NOT NULL DEFAULT NOW(),

  UNIQUE(name)
);

/*
This table holds information for auditing and logging run tasks

 Event though it seems redudant to have this info here since the values in the task_crons->params 
 can change over time this will record the values at the time of run
*/
CREATE TABLE task_crons_history(
  id uuid primary key DEFAULT uuid_generate_v4(),
  task_id uuid REFERENCES task_crons ON UPDATE CASCADE ON DELETE SET NULL,
  name text NOT NULL,
  type text NOT NULL,
  schedule text,
  runtime timestamp,
  action text NOT NULL,
  params jsonb NOT NULL,
  run_date timestamp NOT NULL DEFAULT NOW()
);