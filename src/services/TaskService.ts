import pgPromise from "pg-promise";
import dotenv from 'dotenv';

dotenv.config();

export class TaskService {
  static connection = pgPromise()(process.env.CONNECTION_STRING);

  static getAll() {
    return this.connection.any(`SELECT * FROM task_crons`);
  };

  static getHistoryRuns() {
    return this.connection.any(`SELECT * FROM task_crons_history ORDER BY run_date DESC`);
  };

  static getTask(taskId: string) {
    return this.connection.oneOrNone(
      'SELECT * FROM task_crons WHERE task_id = $(task_id)',
      { task_id: taskId }
    );
  }

  static deleteTask(taskId: string) {
    return this.connection.none(
      'DELETE FROM task_crons WHERE task_id = $(task_id)',
      { task_id: taskId }
    );
  }

  static upsertTask(task: any) { 
    return this.connection.one(`
      INSERT INTO task_crons (task_id, name, type, action, schedule, runtime, params)
      VALUES (
        COALESCE($(task_id), uuid_generate_v4()),
        $(name),
        $(type),
        $(action),
        $(schedule),
        $(runtime),
        $(params)
      )
      ON CONFLICT (task_id) 
      DO UPDATE SET 
        name = $(name),
        type = $(type),
        action = $(action),
        schedule = $(schedule),
        runtime = $(runtime),
        params = $(params)
      RETURNING task_id`,
      {
        task_id: task.task_id || null,
        name: task.name,
        type: task.type,
        action: task.action,
        schedule: task.schedule,
        runtime: task.runtime,
        params: task.params,
      }
    );
  }
}