INSERT INTO task_crons (name, type, action, schedule, params)
VALUES 
  (
    'My First Test Task',
    'one-time',
    'notification',
    null,
    '{
      "type": "email",
      "to": "sabatinipeter@gmail.com",
      "subject": "This is a test",
      "body": "Hello, This is a test"
    }'
  ),
  (
    'Take Diego to Soccer Practice',
    'recurring',
    'notification',
    '0 17 * * 1,3',
    '{
      "type": "email",
      "to": "sabatinipeter@gmail.com",
      "subject": "Take Diego to Soccer Practice",
      "body": "Address location ..."
    }'
  );
